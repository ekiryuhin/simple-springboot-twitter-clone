package com.ekiryuhin.SpringTest2.controllers;

import com.ekiryuhin.SpringTest2.domain.Role;
import com.ekiryuhin.SpringTest2.domain.User;
import com.ekiryuhin.SpringTest2.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ProfileController {
    private final UserService userService;

    @Autowired
    public ProfileController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/profile/{user}")
    public String userProfile(@PathVariable User user, @AuthenticationPrincipal User logUser, Model model) {

        model.addAttribute("logUser", logUser);
        model.addAttribute("user", user);
        model.addAttribute("roles", Role.values());
        return "profile";
    }

    @PostMapping("/profile/{user}")
    public String saveProfile(@PathVariable User user,
                              @RequestParam String email,
                              @RequestParam String password
    ) {

        userService.updateProfile(user, email, password);

        return "profile";
    }

}
