package com.ekiryuhin.SpringTest2.controllers;

import com.ekiryuhin.SpringTest2.domain.User;
import com.ekiryuhin.SpringTest2.domain.dto.CaptchaResponseDto;
import com.ekiryuhin.SpringTest2.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.util.Collections;
import java.util.Map;

@Controller
public class RegistrationController {

    private final static String RECAPTCHA_URL ="https://www.google.com/recaptcha/api/siteverify?secret=%s&response=%s";

    private final
    UserService userService;

    private final
    RestTemplate restTemplate;

    @Value("${recaptcha.secret}")
    String recaptchaSecret;

    @Autowired
    public RegistrationController(UserService userService, RestTemplate restTemplate) {
        this.userService = userService;
        this.restTemplate = restTemplate;
    }

    @GetMapping("/registration")
    public String registration() {
        return "registration";
    }

    @PostMapping("/registration")
    public String addUser(@RequestParam("confPassword") String confPassword,
                          @RequestParam("g-recaptcha-response") String captchaResponse,
                          @Valid User user,
                          BindingResult bindingResult,
                          Model model) {

        String url = String.format(RECAPTCHA_URL, recaptchaSecret, captchaResponse);
        CaptchaResponseDto response = restTemplate.postForObject(url, Collections.emptyList(), CaptchaResponseDto.class);

        if (!response.isSuccess()) {
            model.addAttribute("captchaError", "Fill captcha");
        }

        boolean isConfirmEmpty = StringUtils.isEmpty(confPassword);
        if (isConfirmEmpty)
            model.addAttribute("passwordConfError", "Password confirmation cannot be empty");

        boolean isPasswordConfirmFailed = user.getPassword() != null && !user.getPassword().equals(confPassword);
        if (isPasswordConfirmFailed) {
            model.addAttribute("passwordConfError", "Passwords are different");
        }

        if (isConfirmEmpty || bindingResult.hasErrors() || !response.isSuccess() || isPasswordConfirmFailed) {
            Map<String, String> errors = ControllerUtils.getErrors(bindingResult);
            model.mergeAttributes(errors);
            return "registration";
        }

        if (!userService.addUser(user)) {
            model.addAttribute("usernameError", "User exists!");
            return "registration";
        }
        return "redirect:/login";
    }

    @GetMapping("/activate/{code}")
    public String activate(Model model, @PathVariable String code) {
        boolean isActivated = userService.activateUser(code);
        if (isActivated) {
            model.addAttribute("message", "User activated");
            model.addAttribute("messageType", "success");
        } else {
            model.addAttribute("message", "Activation code is not found");
            model.addAttribute("messageType", "danger");
        }
        return "login";
    }

}
