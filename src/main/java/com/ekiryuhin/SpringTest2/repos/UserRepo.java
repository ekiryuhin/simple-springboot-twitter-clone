package com.ekiryuhin.SpringTest2.repos;

import com.ekiryuhin.SpringTest2.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepo extends JpaRepository<User, Long> {
    User findByUsername(String username);

    User findByActivationCode(String code);
}
