<#macro info>
    <div class="card shadow p-3 mb-5 bg-white rounded" style="width: 18rem;">
        <div class="card-body">
    <h2 class="card-title">${user.username}</h2>
    <h5 class="card-subtitle mb-2 text-muted">ID#${user.id}</h5>
    <#assign
    isActive = user.isActive()
    isAdmin = user.isAdmin()
    />
        <#if isAdmin>
        <h5 class="card-subtitle mb-2 text-muted">Admin</h5>
        </#if>
    <#if isActive>
        <h5 class="card-subtitle mb-2 text-muted">Active</h5>
    </#if>
        </div>
    </div>
</#macro>