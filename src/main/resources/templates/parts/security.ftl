<#assign known = Session.SPRING_SECURITY_CONTEXT??>
<#if known>
    <#assign
    isLogin = true
    currentUser = Session.SPRING_SECURITY_CONTEXT.authentication.principal
    name = currentUser.getUsername()
    isAdmin = currentUser.isAdmin()>
<#else>
    <#assign
    isLogin = false
    name = "unknown"
    isAdmin = false
    >
</#if>