<#include "security.ftl">

<nav class="navbar navbar-expand-lg navbar-light bg-light shadow-sm p-3 mb-5 bg-white rounded">
    <a class="navbar-brand" href="/">MainPage</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
        <#if isLogin>
            <li class="nav-item">
                <a class="nav-link" href="/">Home</a>
            </li>
            <#if isAdmin>
            <li class="nav-item">
                <a class="nav-link" href="/user">Users</a>
            </li>
            </#if>
            <li class="nav-item">
                <a class="nav-link" href="/profile/${currentUser.id}">Profile</a>
            </li>
            <li class="nav-item">
                <form  action="/logout" method="post">
                    <input type="hidden" name="_csrf" value="${_csrf.token}"/>
                    <input class="btn btn-outline-danger" type="submit" value="LogOut">
                </form>
            </li>
        <#else>
            <li class="nav-item">
                <a class="nav-link" href="/login">LogIn</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/registration">Registration</a>
            </li>
        </#if>
    </ul>
         <#if isLogin>
        <div class="navbar-text">${name}</div>
         </#if>
    </div>
</nav>