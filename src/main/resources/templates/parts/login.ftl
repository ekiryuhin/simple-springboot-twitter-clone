<#macro logForm path isRegisterForm>
<form action="${path}" method="post">
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">User Name:</label>
        <div class="col-sm-5">
            <input class="form-control ${(usernameError??)?string('is-invalid', '')}"
                   type="text" placeholder="Username" name="username"
                   value="<#if user??>${user.username}</#if>"/>
            <#if usernameError??>
                <div class="invalid-feedback">
                    ${usernameError}
                </div>
            </#if>
        </div>
    </div>
    <#if isRegisterForm>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Email:</label>
        <div class="col-sm-5">
            <input class="form-control ${(emailError??)?string('is-invalid', '')}"
                   type="email" name="email" placeholder="example@ex.com"
                   value="<#if user??>${user.email}</#if>"/>
            <#if emailError??>
                <div class="invalid-feedback">
                    ${emailError}
                </div>
            </#if>
        </div>
    </div>
    </#if>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Password:</label>
        <div class="col-sm-5">
            <input class="form-control ${(passwordConfError??)?string('is-invalid', '')}"
                   type="password" name="password"/>
        </div>
    </div>
    <#if isRegisterForm>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Password:</label>
        <div class="col-sm-5">
            <input class="form-control ${(passwordConfError??)?string('is-invalid', '')}"
                   type="password" name="confPassword"/>
            <#if passwordConfError??>
                <div class="invalid-feedback">
                    ${passwordConfError}
                </div>
            </#if>
        </div>
    </div>
    <div>
        <div class="g-recaptcha" data-sitekey="6LeM6mkUAAAAAIY0o8U3s0_9XhhcUtbUB4svqUL3"></div>
        <#if captchaError??>
        <div class="alert alert-danger" role="alert">
            ${captchaError}
        </div>
        </#if>
    </div>
    </#if>
    <input type="hidden" name="_csrf" value="${_csrf.token}"/>
    <input class="btn btn-primary" type="submit" value="Sign In"/>
    <#if !isRegisterForm>
    <a href="/registration">Registration</a>
    </#if>
</form>
</#macro>

<#macro logOut>
    <div>
        <form action="/logout" method="post">
            <input type="hidden" name="_csrf" value="${_csrf.token}"/>
            <input type="submit" value="Sign out">
        </form>
    </div>
</#macro>