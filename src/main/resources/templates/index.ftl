<#import "parts/common.ftl" as cpage>
<#import "parts/login.ftl" as login>

<@cpage.page>

<div class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1 class="display-4">Just my SpringBoot project</h1>
        <p class="lead">This is a page with some messages and pictures</p>
    </div>
</div>

<a class="btn btn-primary" data-toggle="collapse" href="#collapseForm" role="button" aria-expanded="false"
   aria-controls="collapseExample">
    Add new
    <Message></Message>
</a>
<div class="collapse <#if message??>show</#if> shadow p-3 mb-5 bg-white rounded" id="collapseForm">
    <div class="form-group mt-3">
        <form method="post" enctype="multipart/form-data">
            <div class="form-group ml-4 col-sm-6">
                <input type="text" class="form-control ${(textError??)?string('is-invalid', '')}"
                       value="<#if message??>${message.text}</#if>" name="text" placeholder="Введите сообщение:">
                 <#if textError??>
                <div class="invalid-feedback">
                    ${textError}
                </div>
                 </#if>
            </div>
            <div class="form-group ml-4 col-sm-3">
                <input type="text" class="form-control ${(textError??)?string('is-invalid', '')}"
                       value="<#if message??>${message.tag}</#if>" name="tag" placeholder="Тэг">
                <#if tagError??>
                <div class="invalid-feedback">
                    ${tagError}
                </div>
                </#if>
            </div>
            <div class="form-group ml-4 col-sm-3">
                <div class="custom-file">
                    <input hidden="hidden" type="file" name="file" id="customFile">
                    <label class="custom-file-label" for="customFile">Choose file</label>
                </div>
            </div>
            <input type="hidden" name="_csrf" value="${_csrf.token}"/>
            <div class="form-group ml-5">
                <button class="btn btn-outline-success" type="submit">Добавить</button>
            </div>
        </form>
    </div>
</div>

<form class="mt-5" method="get" action="/main">
    <div class="row">
        <div class="col-8">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Find by tag" name="filter"
                       value="${filter?if_exists}">
            </div>
        </div>
        <div class="col-2">
            <button type="submit" class="btn btn-primary">Search</button>
        </div>
    </div>

</form>


<div class="card-columns">
    <#list messages as message>

        <div class="card my-3 shadow p-3 mb-5 bg-white rounded" style="width: 18rem;">
        <#if message.filename??>
        <img class="card-img-top" src="/img/${message.filename}">
        </#if>
            <div class="card-body">
                <h5 class="card-title">#${message.id}</h5>
                <p class="card-text">${message.text}</p>
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item">Tag: ${message.tag}</li>
            </ul>
            <div class="card-body">
                <a href="/profile/${message.authorId}" class="card-link">Author: ${message.authorName}</a>
            </div>

        </div>
    <#else>
No message
    </#list>
</div>
</@cpage.page>