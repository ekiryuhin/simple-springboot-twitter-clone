<#import "parts/common.ftl" as cpage>
<#import "parts/userdata.ftl" as userdata>
<@cpage.page>
    <@userdata.info/>
    <#include "parts/security.ftl">
    <#assign
    selfProfile = (currentUser.id == user.id)
    />
    <#if selfProfile>
    <h2>Edit</h2>
    <form method="post" class="col-sm-6">
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Email:</label>
            <div class="col-sm-10">
                <input type="email" name="email"  class="form-control" value=${user.email!''}>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Password:</label>
            <div class="col-sm-10">
                <input type="password" name="password"  class="form-control">
            </div>
        </div>
        <div class="text-right">
            <input type="hidden" name="_csrf" value = "${_csrf.token}"/>
            <input class="btn btn-primary" type="submit" value="Save"/>
        </div>
    </form>
    </#if>
</@cpage.page>