<#import "parts/common.ftl" as cpage>

<@cpage.page>
<h1>User list</h1>
<table class="table shadow p-3 mb-5 bg-white rounded">
    <thead>
    <tr>
        <th scope="col">Name</th>
        <th scope="col">Role</th>
        <th scope="col"></th>
    </tr>
    </thead>
    <tbody>
    <#list users as user>
    <tr>
        <td><a href="/profile/${user.id}">${user.username}</a></td>
        <td><#list user.roles as role>${role}<#sep>, </#list></td>
        <td><a href="/user/${user.id}">edit</a></td>
    </tr>
    </#list>
    </tbody>
</table>
</@cpage.page>
