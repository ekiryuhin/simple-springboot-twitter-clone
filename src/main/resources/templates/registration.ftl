<#import "parts/common.ftl" as cpage>
<#import "parts/login.ftl" as logForm>
<@cpage.page>
    <h1>Registration</h1>
    <@logForm.logForm "/registration" true />
</@cpage.page>