<#import "parts/common.ftl" as cpage>

<@cpage.page>
    <div class="shadow p-3 mb-5 bg-white rounded">
    <h1>User editor</h1>
    <form action="/user" method="post">
        <input type="hidden" name="userId" value="${user.id}">
        <div class="form-group mt-4 col-sm-4">
            <input type="text" class="form-control" name="username" value="${user.username}">
        </div>
        <#list roles as role>
            <div class="form-group form-check ml-4">
                <label><input type="checkbox" class="form-check-input" name="${role}" ${user.roles?seq_contains(role)?string("checked", "")}>${role}</label>
            </div>
        </#list>
        <input type="hidden" name="_csrf" value = "${_csrf.token}"/>
        <button type="submit" class="btn btn-outline-warning btn-lg ml-4" >Save</button>
    </form>
    </div>
</@cpage.page>