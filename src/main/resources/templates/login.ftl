<#import "parts/common.ftl" as cpage>
<#import "parts/login.ftl" as logForm>
<@cpage.page>
<h1>Login page</h1>
    <#if Session?? && Session.SPRING_SECURITY_LAST_EXCEPTION??>
    <div class="alert alert-danger" role="alert">
        ${Session.SPRING_SECURITY_LAST_EXCEPTION.message}
    </div>
    </#if>
    <#if message??>
    <div class="alert alert-${messageType}" role="alert">
        ${message}
    </div>
    </#if>
    <@logForm.logForm "/login" false/>
</@cpage.page>